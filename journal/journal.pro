#-------------------------------------------------
#
# Project created by QtCreator 2017-03-25T19:43:16
#
#-------------------------------------------------

QT += core gui widgets

TARGET = Journal
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    papermodel.cpp \
    tool.cpp \
    selecttool.cpp \
    texttool.cpp \
    textbox.cpp \
    paperlist.cpp \
    paperview.cpp \
    papercontroller.cpp \
    mpbrushselector.cpp \
    mpbrush.cpp \
    mpbrushlib.cpp \
    mphandler.cpp \
    mpsurface.cpp \
    mptile.cpp \
    brushtool.cpp

HEADERS  += mainwindow.h \
    papermodel.h \
    tool.h \
    selecttool.h \
    texttool.h \
    textbox.h \
    paperlist.h \
    huge_dictionary.h \
    papercontroller.h \
    paperview.h \
    mpbrushselector.h \
    mpbrush.h \
    mpbrushlib.h \
    mphandler.h \
    mpsurface.h \
    mptile.h \
    brushtool.h

FORMS    += mainwindow.ui

DISTFILES += \
    README.md \
    uml.qmodel

RESOURCES += \
    assets.qrc

LIBS += -L$$OUT_PWD/../json-c -ljson-c
LIBS += -L$$OUT_PWD/../libmypaint -lmypaint

# --- json-c ---
win32:CONFIG(release, debug|release): LIBS += -L../json-c/release/ -ljson-c
else:win32:CONFIG(debug, debug|release): LIBS += -L../json-c/debug/ -ljson-c
else:unix: LIBS += -L../json-c -ljson-c

INCLUDEPATH += ../json-c
DEPENDPATH += ../json-c

INCLUDEPATH += ../libmypaint
